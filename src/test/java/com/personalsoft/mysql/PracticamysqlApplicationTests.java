package com.personalsoft.mysql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.personalsoft.mysql.controler.UserController;
import com.personalsoft.mysql.model.db.UserEntity;
import com.personalsoft.mysql.model.dto.UserDto;
import com.personalsoft.mysql.repository.UserDao;
import com.personalsoft.mysql.service.UserService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PracticamysqlApplication.class)
@WebMvcTest({UserController.class, UserService.class})

class PracticamysqlApplicationTests {
	
	private static final Logger logger = LoggerFactory.getLogger(PracticamysqlApplicationTests.class);
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	UserController userControler;
	
	@Autowired
	MockMvc mock;
	
	@MockBean
	UserDao dao;
	
	UserDto userDto;

	@BeforeEach
	void contextLoads() { 
		//userControler.save(null); 
		userDto = UserDto.builder().id(1).name("oscar Hernandez").email("oscar@oscar.com.co").age(30).build();
	}
	
	@Test
	void user_UT01_CreateUserSuccess_ReturnOkAndAnUser() throws Exception {
		logger.info("user_UT01_CreateUserSuccess_ReturnOkAndAnUser()");
		// GIVEN
		//UserDto userDto = UserDto.builder().name("oscar Hernandez").email("oscar@oscar.com.co").age(20).build();
		//UserEntity userEntityReq = UserEntity.builder().name("oscar Hernandez").email("oscar@oscar.com.co").age(20).build();
		//userDto.setName("Ximena Hernandez");
		UserEntity userEntityRes = UserEntity.builder().id(1).name("oscar Hernandez").email("oscar@oscar.com.co").age(20).build();
		
		when(dao.save(any(UserEntity.class))).thenReturn(userEntityRes);
		
		// WHEN
//		UserEntity userEntity = userControler.save(userDto);
		MvcResult mvcRes = getResultPost(userDto);
		String userEntityJson = mvcRes.getResponse().getContentAsString();
		UserEntity userEntity = mapper.readValue(userEntityJson, UserEntity.class);
		// THEN
		assertEquals("oscar Hernandez", userEntity.getName());
		assertEquals(userEntity.getEmail(), "oscar@oscar.com.co");
		assertNotNull(userEntity.getId());
		assertTrue(userEntity.getAge() >= 18);
	}
	
	@Test
	void user_UT02_UpdateUserSuccess_ReturnOkAndAnUser() throws Exception {
		// GIVEN
		
		UserEntity userEntityResBd = UserEntity.builder().id(1).name("oscar Hernandez").email("oscar@oscar.com.co").age(26).build();
		
		when(dao.findById(1)).thenReturn(Optional.of(userEntityResBd));
		
		UserEntity userEntityRes = UserEntity.builder().id(1).name("oscar Hernandez").email("oscar@oscar.com.co").age(30).build();
		
		when(dao.save(any(UserEntity.class))).thenReturn(userEntityRes);
		// WHEN
		MvcResult mvcRes = getResultPut(userDto);
		String userEntityJson = mvcRes.getResponse().getContentAsString();
		UserEntity userEntity = mapper.readValue(userEntityJson, UserEntity.class);
		// THEN
		assertNotNull(userEntity);
		assertTrue(userEntity.getAge() >= 25);
		assertTrue(userEntityResBd.getAge() >= 25);
		assertTrue(userDto.getAge() >= 25);
	}
	
	@Test
	void user_UT03_UpdateUserNoSuccess_ReturnNull() throws Exception {
		// GIVEN		
		UserEntity userEntityResBd = UserEntity.builder().id(1).name("oscar Hernandez").email("oscar@oscar.com.co").age(24).build();
		
		when(dao.findById(1)).thenReturn(Optional.of(userEntityResBd));
		
		
		when(dao.save(any(UserEntity.class))).thenReturn(null);
		// WHEN
		MvcResult mvcRes = getResultPut(userDto);
		String userEntityJson = mvcRes.getResponse().getContentAsString();
		// THEN
		assertTrue(userEntityJson.isEmpty());
		assertTrue(userEntityResBd.getAge() < 25);
	}
	
	@Test
	void user_UT04_ListUserSuccess_ReturnOkListUserEntity() throws Exception {
		// GIVEN		
		UserEntity userEntityResBd = UserEntity.builder().id(1).name("oscar Hernandez").email("oscar@oscar.com.co").age(24).build();
		
		List<UserEntity> userList = new ArrayList<>();
		
		userList.add(userEntityResBd);		
		
		when(dao.findAll()).thenReturn(userList);
		// WHEN
		MvcResult mvcRes = getResultGet();
		String userEntityJson = mvcRes.getResponse().getContentAsString();
		List<UserEntity> listUserEntity = (ArrayList<UserEntity>) mapper.readValue(userEntityJson, ArrayList.class);
		// THEN
		assertFalse(listUserEntity.isEmpty());
		assertEquals(userList.size(), listUserEntity.size());
	}
	
	private MvcResult getResultPost(UserDto requestObject) throws Exception {
		String json = mapper.writeValueAsString(requestObject);

		return this.mock.perform(post("/")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
			.andReturn();
	}
	
	private MvcResult getResultPut(UserDto requestObject) throws Exception {
		String json = mapper.writeValueAsString(requestObject);

		return this.mock.perform(put("/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
			.andReturn();
	}
	
	private MvcResult getResultGet() throws Exception {
		return this.mock.perform(get("/")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				)
			.andReturn();
	}
	

}
