package com.personalsoft.mysql.controler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.personalsoft.mysql.model.db.UserEntity;
import com.personalsoft.mysql.model.dto.UserDto;
import com.personalsoft.mysql.service.UserService;

@RestController
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	@GetMapping
	@ResponseBody
	public List<UserEntity> getAll(){		
		return userService.userList();				
	}
	
	@PostMapping
	@ResponseBody
	public UserEntity save(@Valid @RequestBody UserDto userDto){
		logger.info("Crendo usuario");
		logger.warn("Advertencia de usuario");
		logger.error("Error de Usuario");
		if(userDto == null) return null;		
		return userService.create(userDto);			
	}
	
	@PutMapping("/{id}")
	@ResponseBody
	public UserEntity update(
			@Valid
			@RequestBody UserDto userDto, 
			@PathVariable Integer id
			){
		logger.info("update a User: {}", userDto );
		UserEntity userEntity = userService.findById(id);
		if(userEntity != null && userEntity.getAge() >= 25) {
			userDto.setEmail(userEntity.getEmail());
			return userService.update(userDto, id);
		}
		return null;
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}
	
}
