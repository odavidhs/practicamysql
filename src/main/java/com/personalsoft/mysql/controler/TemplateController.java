package com.personalsoft.mysql.controler;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TemplateController {
	
	@GetMapping("/index")
	public String getAll(Model model){	
		
		return "index";		
	}

}
