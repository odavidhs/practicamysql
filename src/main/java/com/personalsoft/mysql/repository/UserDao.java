package com.personalsoft.mysql.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.personalsoft.mysql.model.db.UserEntity;

@Repository
public interface UserDao extends CrudRepository<UserEntity, Integer> {
	
}
