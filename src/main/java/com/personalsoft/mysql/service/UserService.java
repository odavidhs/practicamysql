package com.personalsoft.mysql.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.personalsoft.mysql.model.db.UserEntity;
import com.personalsoft.mysql.model.dto.UserDto;
import com.personalsoft.mysql.repository.UserDao;

@Service
public class UserService {
	
	@Autowired
	UserDao userDao;
	
	public List<UserEntity> userList() {
		return (List<UserEntity>)userDao.findAll();		
		
	}
	
	public UserEntity create(UserDto userDto) {
		UserEntity userEntity = new UserEntity();
		userEntity.setName(userDto.getName());
		userEntity.setEmail(userDto.getEmail());
		return userDao.save(userEntity);
	}
	
	public UserEntity update(UserDto userDto, Integer id) {
		// get the user, if it doesn't exist put null
		UserEntity userEntity = userDao.findById(id).orElse(null);	
		
		if(userEntity != null) {
			userEntity.setName(userDto.getName());
			userEntity.setEmail(userDto.getEmail());
			userDao.save(userEntity);
		}		
		
		return userEntity;
		
	}
	
	public UserEntity findById(Integer id) {
		return userDao.findById(id).orElse(null);
	}
}
