package com.personalsoft.mysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.Generated;

@SpringBootApplication
@Generated
public class PracticamysqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticamysqlApplication.class, args);
	}

}
